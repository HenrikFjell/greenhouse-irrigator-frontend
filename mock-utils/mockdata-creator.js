var LocalTime = require("@js-joda/core").LocalTime;
var Duration = require("@js-joda/core").Duration;

function getSchedules() {
  const tomatoes = {
    loopName: 'Tomater',
    scheduleItems: [{
      time: LocalTime.of(8, 0),
      duration: Duration.ofSeconds(180)
    },
      {
        time: LocalTime.of(12, 0),
        duration: Duration.ofSeconds(180)
      },
      {
        time: LocalTime.of(20, 0),
        duration: Duration.ofSeconds(60)
      }
    ]
  };

  const cucumbers = {
    loopName: 'Gurkor',
    scheduleItems: [{
      time: LocalTime.of(7, 0),
      duration: Duration.ofSeconds(60)
    },
      {
        time: LocalTime.of(21, 0),
        duration: Duration.ofSeconds(120)
      }
    ]
  };

  return [tomatoes, cucumbers];
}


module.exports = {
  getSchedules: getSchedules
}
