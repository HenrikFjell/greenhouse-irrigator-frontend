import { Duration, LocalTime } from '@js-joda/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Schedule } from '../src/app/schedule';
import { SchedulesService } from '../src/app/schedules.service';
import { getMockSchedules } from './mockdata-creator';

export class SchedulesServiceMock extends SchedulesService {

    constructor() {
        super();
    }

    getSchedules(): Observable<Schedule[]> {
        return new Observable<Schedule[]>(
            subscriber => {
                subscriber.next(getMockSchedules());
            }).pipe(map(this.createJoda));
    }

    saveSchedules(schedules: Schedule[]): Observable<Schedule[]> {
        return new Observable<Schedule[]>(
            subscriber => {
                subscriber.next(schedules);
            }).pipe(map(this.createJoda));
    }

    private createJoda(schedules: Schedule[]): Schedule[] {
        schedules.forEach((schedule) => {
            schedule.scheduleItems.forEach((scheduleItem) => {

                const durationString = scheduleItem.duration.toString();
                scheduleItem.duration = Duration.parse(durationString);

                const timeString = scheduleItem.time.toString();
                scheduleItem.time = LocalTime.parse(timeString);
            });
        });
        return schedules;
    }
}
