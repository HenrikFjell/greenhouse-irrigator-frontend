var express = require("express");
var app = express();
app.use(express.json());


var mockdata = require('./mockdata-creator.js');
var localSchedules;

app.listen(3000, () => {
    console.log("Local backend mock server running on port 3000");
});

app.get('/api/schedules', (req, res, next) => {
    const schedules = mockdata.getSchedules();
    return res.status(200).send(schedules);
});

app.put('/api/schedules', (req, res, next) => {
    console.log('Got a request: ' + req.body.loopName);
    return res.status(200).send(JSON.stringify(req.body));
});
