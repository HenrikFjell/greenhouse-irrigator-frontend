import { NgModule } from '@angular/core';
import { FaIconLibrary, FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { faTrashAlt as farTrashAlt } from '@fortawesome/free-regular-svg-icons';
import { faPlus as fasPlus } from '@fortawesome/free-solid-svg-icons';

@NgModule({
  declarations: [],
  imports: [FontAwesomeModule],
  exports: [FontAwesomeModule]
})
export class IconsModule {
  constructor(private library: FaIconLibrary) {
    library.addIcons(farTrashAlt, fasPlus);
  }
}
