import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {ScheduleItemComponent} from './schedule-item.component';
import {getMockSchedules} from '../../../mock-utils/mockdata-creator';
import {Schedule} from '../schedule';
import {AppModule} from '../app.module';
import {Duration, LocalTime} from '@js-joda/core';


describe('ScheduleItemComponent', () => {
  let component: ScheduleItemComponent;
  let fixture: ComponentFixture<ScheduleItemComponent>;
  const schedules: Schedule[] = getMockSchedules();

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ScheduleItemComponent ],
      imports: [ AppModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScheduleItemComponent);
    component = fixture.componentInstance;
    component.scheduleItem = schedules[0].scheduleItems[0];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should update schedule item on duration input', () => {
    const duration = fixture.nativeElement.querySelector('#duration');
    duration.value = 5;

    const event = createNewEvent('input');
    duration.dispatchEvent(event);

    expect(component.scheduleItem.duration).toEqual(Duration.ofMinutes(5));
   });

  it('should update schedule item on time input', () => {
    const timeField = fixture.nativeElement.querySelector('#time');
    const timeValue = LocalTime.of(12, 42);
    timeField.value = timeValue;

    const event = createNewEvent('input');
    timeField.dispatchEvent(event);

    expect(component.scheduleItem.time).toEqual(timeValue);
   });

  it('should emit index on add', () => {
      const spy = spyOn(component.addIndex, 'emit');

      component.add();

      expect(spy).toHaveBeenCalledWith(component.itemIndex);
   });

  it('should emit index on delete', () => {
    const spy = spyOn(component.deleteIndex, 'emit');

    component.delete();

    expect(spy).toHaveBeenCalledWith(component.itemIndex);
 });


  function createNewEvent(eventName: string, bubbles = false, cancelable = false) {
    const evt = document.createEvent('CustomEvent');
    evt.initCustomEvent(eventName, bubbles, cancelable, null);
    return evt;
  }
});
