import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup } from '@angular/forms';
import { Duration, LocalTime, DateTimeFormatter } from '@js-joda/core';
import { ScheduleItem } from '../schedule';

@Component({
  selector: 'app-schedule-item',
  templateUrl: './schedule-item.component.html',
  styleUrls: ['./schedule-item.component.scss']
})
export class ScheduleItemComponent implements OnInit {

  @Input() scheduleItem!: ScheduleItem;
  @Input() itemIndex: number = -1;
  @Output() deleteIndex = new EventEmitter<number>();
  @Output() addIndex = new EventEmitter<number>();


  scheduleItemForm = new UntypedFormGroup({
    time: new UntypedFormControl(''),
    duration: new UntypedFormControl('')
  });

  ngOnInit(): void {
    this.scheduleItemForm.setValue({
      time: this.scheduleItem.time.format(DateTimeFormatter.ofPattern('HH:mm')),
      duration: this.scheduleItem.duration.toMinutes()
    });

    this.scheduleItemForm.valueChanges.subscribe(
      value => {
        this.scheduleItem.duration = Duration.ofMinutes(value.duration);
        this.scheduleItem.time = LocalTime.parse(value.time);
      }
    );
  }

  delete(): void {
     this.deleteIndex.emit(this.itemIndex);
  }

  add(): void {
    this.addIndex.emit(this.itemIndex);
  }

}
