import {LocalTime, Duration} from '@js-joda/core';

export interface Schedule {

    loopName: string;
    scheduleItems: ScheduleItem[];
}

export interface ScheduleItem {

    time: LocalTime;
    duration: Duration;
}
