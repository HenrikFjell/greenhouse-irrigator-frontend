import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { LocalTime } from '@js-joda/core';
import { getMockSchedules } from '../../../mock-utils/mockdata-creator';
import { AppModule } from '../app.module';
import { Schedule } from '../schedule';
import { ScheduleComponent } from './schedule.component';

describe('ScheduleComponent', () => {
  let component: ScheduleComponent;
  let fixture: ComponentFixture<ScheduleComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ScheduleComponent],
      imports: [AppModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScheduleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    const schedules = getMockSchedules() as Schedule[];
    component.schedule = schedules[0];

  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should add a new schedule item at begin', () => {
    component.add(0);
    expect(component.schedule.scheduleItems[1]).toEqual(component.schedule.scheduleItems[0]);
  });

  it('should add a new schedule item at end', () => {
    component.add(1);
    expect(component.schedule.scheduleItems[2]).toEqual(component.schedule.scheduleItems[1]);
  });

  it('should delete one item', () => {
    component.delete(0);
    expect(component.schedule.scheduleItems[0].time).toBe(LocalTime.of(12, 0));
  });

  it('should emit when all items deleted', () => {
    const scheduleIndex = 3;
    component.scheduleIndex = scheduleIndex;
    const spy = spyOn(component.deleteIndex, 'emit');

    for (let i = 0; i < 3; i++) {
      component.delete(0);
    }

    expect(spy).toHaveBeenCalledWith(scheduleIndex);
  });

});
