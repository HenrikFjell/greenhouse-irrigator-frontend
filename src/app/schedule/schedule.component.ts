import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Schedule, ScheduleItem } from '../schedule';

@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.scss']
})
export class ScheduleComponent {

  @Input() schedule!: Schedule;
  @Input() scheduleIndex: number = -1;
  @Output() deleteIndex = new EventEmitter<number>();

  constructor() { }

  add(index: number): void {
    const scheduleItem = this.schedule.scheduleItems[index];
    const scheduleItemCopy: ScheduleItem = { duration: scheduleItem.duration, time: scheduleItem.time };
    this.schedule.scheduleItems.splice(index, 0, scheduleItemCopy);
  }

  delete(index: number): void {
    const scheduleItems = this.schedule.scheduleItems;
    scheduleItems.splice(index, 1);
    if (scheduleItems.length === 0) {
      this.deleteIndex.emit(this.scheduleIndex);
    }
  }

}
