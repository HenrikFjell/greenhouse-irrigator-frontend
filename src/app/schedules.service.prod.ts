import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Duration, LocalTime } from '@js-joda/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Schedule } from './schedule';
import { SchedulesService } from './schedules.service';

@Injectable({
  providedIn: 'root'
})
export class SchedulesServiceProd extends SchedulesService {

  constructor(private httpClient: HttpClient) {
    super();
  }

  getSchedules(): Observable<Schedule[]> {
    const response = this.httpClient.get<Schedule[]>('/api/schedules').pipe(map(this.createJodaSchedules));
    return response;
  }

  saveSchedules(schedules: Schedule[]): Observable<Schedule[]> {
    return this.httpClient
      .put('/api/schedules', schedules)
      .pipe(map(response => response as Schedule[]),
        map(this.createJodaSchedules));
  }

  private createJodaSchedules(schedules: Schedule[]): Schedule[] {
    schedules.forEach((schedule) => {
      schedule = createJodaSchedule(schedule);
    });
    return schedules;
  }

}

const createJodaSchedule = (schedule: Schedule) => {
  schedule.scheduleItems.forEach((scheduleItem) => {

    const durationString = scheduleItem.duration.toString();
    scheduleItem.duration = Duration.parse(durationString);

    const timeString = scheduleItem.time.toString();
    scheduleItem.time = LocalTime.parse(timeString);
  });
  return schedule;
};
