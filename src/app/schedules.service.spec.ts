import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { getMockSchedules } from '../../mock-utils/mockdata-creator';
import { Schedule } from './schedule';
import { SchedulesServiceProd } from './schedules.service.prod';


describe('SchedulesService', () => {
  let service: SchedulesServiceProd;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });

    service = TestBed.inject(SchedulesServiceProd);
    httpTestingController = TestBed.inject(HttpTestingController);

  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return an array of schedules', (done: DoneFn) => {
    const schedules: Schedule[] = getMockSchedules();

    service.getSchedules().subscribe(
      incomingSchedules => {
        expect(incomingSchedules).toEqual(schedules);
        done();
      }
    );

    const incomingRequest = httpTestingController.expectOne('/api/schedules');
    expect(incomingRequest.request.method).toBe('GET');
    incomingRequest.flush(schedules);
    httpTestingController.verify();
  });

  it('should contain Joda duration objects', (done: DoneFn) => {
    service.getSchedules().subscribe(
      incomingSchedules => {
        expect(incomingSchedules[0].scheduleItems[0].duration.toMinutes()).toBe(3);
        done();
      }
    );

    const schedules: Schedule[] = getMockSchedules();
    const incomingRequest = httpTestingController.expectOne('/api/schedules');
    incomingRequest.flush(schedules);
  });

  it('should contain Joda time objects', (done: DoneFn) => {
    service.getSchedules().subscribe(
      incomingSchedules => {
        expect(incomingSchedules[0].scheduleItems[0].time.hour()).toEqual(8);
        done();
      }
    );

    const schedules: Schedule[] = getMockSchedules();
    const incomingRequest = httpTestingController.expectOne('/api/schedules');
    incomingRequest.flush(schedules);
  });

});
