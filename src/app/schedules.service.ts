import { Observable } from 'rxjs';
import { Schedule } from './schedule';

export abstract class SchedulesService {

    abstract getSchedules(): Observable<Schedule[]>;
    abstract saveSchedules(schedules: Schedule[]): Observable<Schedule[]>;
}
