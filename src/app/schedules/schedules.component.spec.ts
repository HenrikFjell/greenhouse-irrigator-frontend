import { Duration, LocalTime } from '@js-joda/core';
import { SchedulesServiceMock } from 'mock-utils/schedules.service.mock';
import { Observable } from 'rxjs';
import { getMockSchedules } from '../../../mock-utils/mockdata-creator';
import { Schedule } from '../schedule';
import { SchedulesComponent } from './schedules.component';


describe('SchedulesComponent', () => {
  let component: SchedulesComponent;

  beforeEach(() => {
    component = new SchedulesComponent(new SchedulesServiceMock());
    component.ngOnInit();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should provide schedules from the Schedules service', () => {
    expect(component.schedules).toEqual(getMockSchedules());
  });

  it('should handle error from Schedules service', () => {
    const observableError: Observable<Schedule[]> = new Observable(observer => observer.error({ message: 'Expected error' }));
    spyOn(component.schedulesService, 'getSchedules').and.returnValue(observableError);

    component.ngOnInit();

    expect(component.schedules).toEqual([]);
  });

  it('should sort schedules on save', () => {
    expect(component.schedules[0].loopName).toBe('Tomater');

    component.save();

    expect(component.schedules[1].loopName).toBe('Tomater');
  });

  it('should call Schedules service on save', () => {
    const spy = spyOn(component.schedulesService, 'saveSchedules').and.callThrough();

    component.save();

    expect(spy).toHaveBeenCalledWith(component.schedules);
  });

  it('should create a new schedule on newSchedule', () => {
    component.newSchedule();

    const theNewSchedule = component.schedules[2];

    expect(theNewSchedule.loopName).toBe('Nytt schema');
    expect(theNewSchedule.scheduleItems[0].duration).toEqual(Duration.ofMinutes(1));
    expect(theNewSchedule.scheduleItems[0].time).toEqual(LocalTime.of(8, 0));
  });

  it('should delete on delete', () => {
    const schedule = component.schedules[1];

    component.delete(0);

    expect(component.schedules[0]).toEqual(schedule);
  });

  it('should cancel a new schedule on cancel', () => {
    component.newSchedule();
    component.cancel();

    expect(component.schedules.length).toBe(2);
  });

  it('should sort schedules correctly', () => {
    const aSchedule: Schedule = { loopName: 'astring', scheduleItems: [] };
    const bSchedule: Schedule = { loopName: 'abtring', scheduleItems: [] };

    // tslint:disable: no-string-literal
    expect(component['compareSchedules'](aSchedule, bSchedule)).toBe(1);
    expect(component['compareSchedules'](bSchedule, aSchedule)).toBe(-1);
    expect(component['compareSchedules'](aSchedule, aSchedule)).toBe(0);
  });

});
