import { Component, OnInit } from '@angular/core';
import { Schedule } from '../schedule';
import { SchedulesService } from '../schedules.service';
import { SchedulesServiceProd } from '../schedules.service.prod';
import { Duration, LocalTime } from '@js-joda/core';

@Component({
  selector: 'app-schedules',
  templateUrl: './schedules.component.html',
  styleUrls: ['./schedules.component.scss'],
  providers: [{ provide: SchedulesService, useClass: SchedulesServiceProd }]
})
export class SchedulesComponent implements OnInit {

  schedules: Schedule[] = [];

  constructor(public schedulesService: SchedulesService) { }

  ngOnInit(): void {
    this.init();
   }

  save(): void {
    this.schedules.sort((a, b) => this.compareSchedules(a, b));
    this.schedulesService.saveSchedules(this.schedules).subscribe(
      result => console.log('Save result is: ' + JSON.stringify(result)),
      error => console.error('Save error is: ' + error.message)
    );
  }

  newSchedule() {
    const newDuration = Duration.ofMinutes(1);
    const newTime = LocalTime.of(8, 0);
    const newSchedule: Schedule = { loopName: 'Nytt schema', scheduleItems: [{ duration: newDuration, time: newTime }] };
    this.schedules.push(newSchedule);
  }

  delete(index: number): void {
    this.schedules.splice(index, 1);
  }

  cancel(): void {
    this.init();
  }

  private compareSchedules(a: Schedule, b: Schedule): number {
    if (a.loopName > b.loopName) {
      return 1;
    }
    if (a.loopName < b.loopName) {
      return -1;
    }
    return 0;
 }

 private init() {
  this.schedulesService.getSchedules().subscribe(
    schedules => this.schedules = schedules,
    error => {
      this.schedules = [];
      console.error('Failed to get schedules: ' + error.message);
    }
  );
 }

}
